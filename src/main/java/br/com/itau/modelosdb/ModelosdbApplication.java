package br.com.itau.modelosdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModelosdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModelosdbApplication.class, args);
	}

}
