package br.com.itau.modelosdb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.modelosdb.models.Modelo;
import br.com.itau.modelosdb.repository.ModeloRepository;

@Service
public class ModeloService {

	@Autowired
	private ModeloRepository modeloRepository;

	public Iterable<Modelo> obterModelos() {
		System.out.println("Chamaram o listar modelos");
		return modeloRepository.findAll();

	}

	public void criarModelo(Modelo modelo) {
		System.out.println(" Chamaram o criar do " + modelo.getNome());
		modeloRepository.save(modelo);
	}
}
