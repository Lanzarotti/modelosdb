package br.com.itau.modelosdb.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.modelosdb.models.Modelo;

@Repository
public interface ModeloRepository extends CrudRepository<Modelo, Long> {

}
