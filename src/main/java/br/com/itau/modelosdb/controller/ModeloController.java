package br.com.itau.modelosdb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.modelosdb.models.Modelo;
import br.com.itau.modelosdb.service.ModeloService;

@RestController
public class ModeloController {

	@Autowired
	private ModeloService modeloService;

	@GetMapping("/modelo")
	public Iterable<Modelo> listarModelos() {
		return modeloService.obterModelos();

	}

	@PostMapping("/modelo")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarModelo(@RequestBody Modelo modelos) {
		modeloService.criarModelo(modelos);
	}

}
